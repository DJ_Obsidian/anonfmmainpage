FROM alpine:3.10 as builder

COPY css /app/css
COPY media /app/media
COPY scripts /app/scripts
COPY data /app/data
ADD index.html /app/

FROM nginx:1.17-alpine
COPY --from=builder /app /usr/share/nginx/html
