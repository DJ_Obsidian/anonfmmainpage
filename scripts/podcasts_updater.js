/* global formatDuration, formatSize, fetchSerializeJson */


/**
 * @param {Array.<string|number>} obj
 * @constructor
 */
function PodcastsIndividual(obj) {
    this.link = obj[0]; // Путь к записи
    this.size = obj[1] * 1024; // Размер файла, в байтах
    this.timestamp = obj[2] * 1000; // Таймстамп
    this.duration = obj[3] * 1000; // Продолжительность
    this.host = obj[4]; // Диджей или hourly
    this.ago = obj[5]; // Как давно был эфир
}

/**
 * @param {Array.<string|number>} obj
 * @constructor
 */
function PodcastsInner(obj) {
    this.pathPart = obj[0]; // Кусок пути. Потому что подкасты и почасовые записи хранятся в разных папках
    this.isHourly = !!obj[1];
    this.categoryTitle = obj[2];
    /** @type {PodcastsIndividual[]} */
    this.records = obj[3]
        .map(rawRecord => new PodcastsIndividual(rawRecord))
        .map(record => {
            record['link'] = this.pathPart + record.link;
            return record;
        });
}

/**
 * @param {PodcastsInner[]} obj
 * @constructor
 */
function Podcasts(obj) {
    this.live = obj[0]; // Записи эфиров
    this.hourlyHigh = obj[1]; // Почасовые записи
    this.hourlyLow = obj[2];
    this.hourlyLowUltra = obj[3];
}

/**
 * Контроллер подкастов
 * Нужен для
 * * управления отображения подкастов
 * @class
 */
function PodcastsContorller() {
    this.podcastsType = 'live';
    const podcatsTypeSelector = document.getElementById('podcastsList');
    podcatsTypeSelector.addEventListener('change', () => {
        this.podcastsType = podcatsTypeSelector.value;
        this._showPodcasts(this.podcasts[this.podcastsType]);
    });
}

/**
 * Загружает список подкастов
 * @return {Promise<PodcastsInner[]>}
 * @private
 */
PodcastsContorller.prototype._getPodcasts = function () {
    return fetchSerializeJson('./data/podcasts.js', PodcastsInner);
};

/**
 * Создает подкасты
 * @param {PodcastsInner[]} podcastsInnerArray
 * @return {Promise<Podcasts>}
 * @private
 */
PodcastsContorller.prototype._constructPodcasts = function (podcastsInnerArray) {
    return new Promise(resolve => resolve(new Podcasts(podcastsInnerArray)));
};

/**
 * Отображает на странице подкасты выбранного типа
 * @param {PodcastsInner} podcasts
 * @private
 */
PodcastsContorller.prototype._showPodcasts = function (podcasts) {
    if (!podcasts || !podcasts.records) {
        return;
    }
    const podcastsContainer = document.getElementById('podcasts').tBodies[0];
    podcastsContainer.innerHTML = '';
    podcasts.records.forEach(podcastsRecord => {
        const containerItem = podcastsContainer.insertRow(-1);
        const recordLink = document.createElement('a');
        recordLink.href = `./records${podcasts.pathPart}${podcastsRecord.link}`;
        recordLink.innerText = podcastsRecord.host;
        containerItem.insertCell().appendChild(recordLink);
        const recordDuration = document.createTextNode(formatDuration(podcastsRecord.duration));
        containerItem.insertCell().appendChild(recordDuration);
        const recordSize = document.createTextNode(formatSize(podcastsRecord.size));
        containerItem.insertCell().appendChild(recordSize);
    });
};

PodcastsContorller.prototype.updatePodcasts = function () {
    this._getPodcasts()
        .then(this._constructPodcasts)
        .then(podcasts => {
            this.podcasts = podcasts;
            this._showPodcasts(this.podcasts[this.podcastsType]);
        });
};
