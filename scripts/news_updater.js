/* global fetchPath */

/**
 * Создает Html элемент отображения новостей
 * @param {NewsItem} newsItem
 * @returns {HTMLAnchorElement}
 * @constructor
 */
function NewsItemView(newsItem) {
    const newsItemContainer = document.createElement('a');
    newsItemContainer.className = 'newsItem';
    newsItemContainer.href = newsItem.link;
    newsItemContainer.target = '_blank';
    const newsItemSource = document.createElement('div');
    newsItemSource.className = 'newsItemSource';
    newsItemSource.textContent = newsItem.source;
    const newsItemDate = document.createElement('div');
    newsItemDate.className = 'newsItemDate';
    newsItemDate.textContent = newsItem.date;
    const newsItemSummary = document.createElement('div');
    newsItemSummary.className = 'newsItemSummary';
    newsItemSummary.textContent = newsItem.summary;

    newsItemContainer.appendChild(newsItemDate);
    newsItemContainer.appendChild(newsItemSource);
    newsItemContainer.appendChild(newsItemSummary);
    return newsItemContainer;
}

/**
 * Создает элемент новстей
 * @param {HTMLElement|ChildNode} newsElement
 * @constructor
 */
function NewsItem(newsElement) {
    this.link = newsElement.childNodes[0].href;
    this.source = newsElement.className;
    this.date = newsElement.childNodes[1].textContent.substring(1, 6);
    this.summary = newsElement.childNodes[1].textContent.replace(/\s+/g, ' ').substring(8);
    this.view = new NewsItemView(this);
}

/**
 * Создает контейнер для новостей
 * @param {string} newsHtmlText
 * @extends {Array}
 * @constructor
 */
function News(newsHtmlText) {
    const parser = new DOMParser();
    const newsElements = parser.parseFromString(newsHtmlText, 'text/html').querySelectorAll('p:not(.more)');
    return [].map.call(newsElements, newsElement => new NewsItem(newsElement));
}

/**
 * @class
 */
function NewsController() {
}

/**
 * Создает из html текста новости для отображения
 * @param {Response} response
 * @returns {Promise<News>}
 * @private
 */
NewsController.prototype._constructNews = function (response) {
    return response.text()
        .then(text => Promise.resolve(new News(text)));
};

/**
 * Показывает новости на странице
 * @param {News} news
 * @private
 */
NewsController.prototype._showNews = function (news) {
    const newsContainer = document.querySelector('#newsBlock > .newsContainer');
    const newsError = document.querySelector('#newsBlock > .newsError');
    newsContainer.innerText = '';
    newsError.innerText = '';
    news.forEach(newsItem => {
        newsContainer.appendChild(newsItem.view);
    });
};

/**
 * Показывает ошибку получения новостей
 * @param {Error} reason
 * @private
 */
NewsController.prototype._showError = function (reason) {
    const newsError = document.querySelector('#newsBlock > .newsError');
    newsError.textContent = 'Не удалось получить новости. Ошибка СТОП 0x0000000b';
    console.error(reason);
};

/**
 * Обновляет новости на странице
 */
NewsController.prototype.updateNews = function () {
    fetchPath('./data/news.html')
        .then(this._constructNews)
        .then(this._showNews, this._showError);
};
