/* Граббер и рендерер секции ответов */

/* global MAIN_SETTINGS, tryParseMd, fetchSerializeJson, titleController */
/* eslint no-unused-vars:0 */

/* jshint unused:false */

/**
 * Придаёт полям из JSON вменяемые имена
 * @param {Array.<string|number>} obj
 * @constructor
 */
function Post(obj) {
    this.isAnswer = obj[0] === '0'; // "0" => true, "1" => false
    this.userId = obj[1];
    this.userText = obj[2];
    this.djId = obj[4];
    this.djText = obj[5];

    const matches = obj[3].match(/(\d+:\d+:\d+).(\d+)/);
    this.time = matches[1];
    const random = +matches[2];
    this.uniqueId = `post_${Math.abs(this.time.hashCode()) + random}`; // Уникальный айди на базе времени
}

/**
 * Отображает посты на страничке
 * @param {Post} post
 */
function showPost(post) {
    if (document.getElementById(post.uniqueId)) {
        return 0;
    }
    const postEl = document.createElement('div');
    postEl.setAttribute('id', post.uniqueId);
    postEl.classList.add('post');

    if (post.isAnswer) {
        postEl.innerHTML = `
<span class="time">${post.time}</span><br>
<span class="userId">${post.userId}</span>: <span class="userText">${post.userText}</span><br>
<span class="djId">${post.djId}</span>: <span class="djText">${post.djText}</span>`;
    } else {
        postEl.innerHTML = `
<span class="time">${post.time}</span><br>
<span class="djId">${post.djId}</span>: <span class="djText">${post.djText}</span>`;
    }

    // Если пост ещё не на глагне, добавляем его туда
    document.getElementById('feed').prepend(postEl);
    return 1;
}

/**
 * @param {Post[]} posts
 * @returns {Number}
 */
function showPosts(posts) {
    return posts.reduce((quantity, post) => {
        return quantity + showPost(post);
    }, 0);
}

/**
 * Устанавливает порядок отображения постов
 * Делоет тексту маркдаун
 * @param {Post[]} posts
 * @returns {Promise<Post[]>}
 */
function preparePosts(posts) {
    return new Promise(resolve => {
        if (MAIN_SETTINGS.newPostsOnTop) {
            posts.reverse(); // Новые посты будут сверху
        }

        const preparedPosts = posts.map(post => {
            if (MAIN_SETTINGS.useMarkdown) {
                post.djText = tryParseMd(post.djText);
                post.userText = tryParseMd(post.userText);
            }
            return post;
        });
        resolve(preparedPosts);
    });
}

function updatePosts() {
    fetchSerializeJson('./data/answers.js', Post)
        .then(preparePosts)
        .then(showPosts)
        .then(newUnread => titleController.addUnreadPosts(newUnread));
}
