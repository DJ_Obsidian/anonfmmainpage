/* global MAIN_SETTINGS */
/* eslint no-unused-vars:0 */

/* jshint unused:false */

const audioStreamLinks = {
    _prefix: `${location.origin}${location.protocol === 'http:' ? ':8000' : '/streams'}/`,
    audio192: 'radio',
    audio64: 'radio-low',
    audio12: 'radio.aac',
};

/**
 * Простенькая обёрточка, шоб удобно було
 * @param {string} name
 * @param {string} url
 * @constructor
 */
function Stream(name, url) {
    this.name = name;
    this.url = url;
}

/**
 * Возвращает удобный массив с объектиками-стримами
 * @returns {Stream[]}
 */
function generateStreams() {
    const streams = [];

    // Да, эта длинная ебала для того, чтобы потом просто и легко править audioStreamLinks
    Object.keys(audioStreamLinks)
        .filter(key => key.startsWith('audio'))
        .forEach(key => {
            const number = key.match(/\d+/)[0];
            const name = `Прямой эфир: ${number} kbps`;
            const url = audioStreamLinks._prefix + audioStreamLinks[key];
            streams.push(new Stream(name, url));
        });

    // А тут мы генерим стримы для машины времени
    for (let hours = 1; hours < 24; hours++) {
        const name = `Машина времени: ${hours} час(ов) назад`;
        const url = `${audioStreamLinks._prefix}timeback-${hours}`;
        streams.push(new Stream(name, url));
    }
    return streams;
}

/**
 * Добавляет в <select> новые option-ы из потоков
 * @param {HTMLSelectElement} streamControllerEl
 * @param {Stream[]} streams
 */
function renderStreams(streamControllerEl, streams) {
    streams.forEach(stream => {
        const optionEl = streamControllerEl.appendChild(document.createElement('option'));
        optionEl.textContent = stream.name;
        optionEl.dataset.src = stream.url;
    });
}

/**
 * Создаёт элемент <audio>, это же очевидно!
 * @param {HTMLElement} playerContainer
 * @returns {HTMLAudioElement}
 */
function createAudioElement(playerContainer) {
    const audioEl = playerContainer.appendChild(document.createElement('audio'));
    audioEl.setAttribute('controls', '');
    audioEl.setAttribute('preload', 'none');
    return audioEl;
}

function createStreamList(playerContainer, audioEl) {
    const streamControllerEl = playerContainer.appendChild(document.createElement('select'));

    streamControllerEl.addEventListener('change', () => {
        MAIN_SETTINGS.selectedStream = streamControllerEl.selectedIndex;

        const link = streamControllerEl.selectedOptions.item(0).dataset.src;
        // При смене потока восстанавливается предыдущее состояние плеера
        const wasPlaying = !audioEl.paused;
        audioEl.setAttribute('src', link);
        if (wasPlaying) {
            audioEl.play();
        }
    });

    renderStreams(streamControllerEl, generateStreams());

    streamControllerEl.selectedIndex = MAIN_SETTINGS.selectedStream;
    // Смена элемента через selectedIndex не вызывает события change.
    // Это странно, но обходится следующим костылём
    streamControllerEl.dispatchEvent(new Event('change'));
}

function initPlayer() {
    const playerContainer = document.getElementById('player');
    const audioEl = createAudioElement(playerContainer);
    createStreamList(playerContainer, audioEl);
}
