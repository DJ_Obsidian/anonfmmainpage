/* Граббер и рендерер расписания */

/* global formatDuration, fetchSerializeJson */
/* eslint no-unused-vars:0 */

/* jshint unused:false */

/**
 * @param {Array.<string|number>} raw
 * @class
 */
function Broadcast(raw) {
    this.begin = 1000 * raw[0];
    this.end = 1000 * raw[1];
    this.host = raw[2];
    this.title = raw[3];

    if (Date.now() < this.begin) {
        this.state = Broadcast.isNotStarted;
    }
    if (Date.now() > this.end) {
        this.state = Broadcast.isEnded;
    }
    if (!this.state) {
        this.state = Broadcast.isOnAir;
    }
}

Broadcast.isNotStarted = 1;
Broadcast.isEnded = 2;
Broadcast.isOnAir = 3;

/**
 * Перерисовывает табличку с расписанием
 * @param {Broadcast[]} broadcasts
 */
function redrawSchedule(broadcasts) {
    const scheduleEl = document.getElementById('schedule').tBodies[0];
    scheduleEl.innerHTML = '';

    broadcasts.forEach(broadcast => {
        let timeClass = '';
        let timeText = '';

        switch (broadcast.state) {
            case Broadcast.isNotStarted:
                timeClass = 'notStarted';
                timeText = `Продолжительность: ${formatDuration(broadcast.begin, broadcast.end)}`;
                break;
            case Broadcast.isOnAir:
                timeClass = 'onAir';
                timeText = `Осталось: ${formatDuration(Date.now(), broadcast.end)}`;
                break;
            case Broadcast.isEnded:
                timeClass = 'ended';
                timeText = 'Передача уже прошла';
                break;
        }
        const beginTime = new Date(broadcast.begin).toLocaleString();

        // Собственно, отображение на страничке
        const newRow = scheduleEl.insertRow(-1);
        newRow.classList.add(timeClass);
        newRow.insertCell().innerHTML = `${beginTime}<br>${timeText}`;
        newRow.insertCell().innerHTML = `${broadcast.title}<br>Ведущий: ${broadcast.host}`;
    });
}

function updateSchedule() {
    fetchSerializeJson('./data/shed.js', Broadcast)
        .then(redrawSchedule);
}
