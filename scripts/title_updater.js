/**
 * Управлятор заголовком страницы
 * Нужен для
 * * подстановки случайного эпитета
 * * контроля за непрочитанными постами
 * @class
 */
function TitleController() {
    this.radioName = this._pageTitles.sample();
    this._changeUnreadPosts(0);

    addEventListener('visibilitychange', () => {
        if (document.visibilityState === 'visible') {
            this._markAllRead();
        }
    });
}

/**
 * Добавляет некоторое количество непрочитанных постов
 * @param {number} unreadQuantity
 */
TitleController.prototype.addUnreadPosts = function (unreadQuantity) {
    if (document.visibilityState !== 'visible') {
        this._changeUnreadPosts(this.unreadPosts + unreadQuantity);
    }
};

TitleController.prototype._markAllRead = function () {
    this._changeUnreadPosts(0);
};

TitleController.prototype._changeUnreadPosts = function (unreadPosts) {
    this.unreadPosts = unreadPosts;
    this._updateTitle();
};

TitleController.prototype._updateTitle = function () {
    let newTitle = `Радио «${this.radioName}»`;
    if (this.unreadPosts) {
        newTitle = `[${this.unreadPosts}] ${newTitle}`;
    }
    document.title = newTitle;
};

TitleController.prototype._pageTitles = [
    'Рефлексивное',
    'Аутичное',
    'Чистое',
    'Дискретность мышления',
    'Когнитивное',
    'Картавое',
    'На самом интересном месте',
    'Интровертное',
    'На семи семплах',
    'Рекурсивное',
    'Утренний израиль',
    'Поехавших людей',
    'Ночные гадости',
    'Кокаинум',
    'GLaDOS',
    'Модуль битардска',
    'Омич',
    'Анонимус',
    'Упоротый школьник',
    'Школонимус',
    'Чан',
    'Веобу',
    'Патлота',
    'Джазфажное',
    'Электронное',
    'Тёпло-ламповое',
    'Брутально-бессердечное',
    'Популярное',
    'MPD',
    'Прыщеблядское',
    'Вендоблядское',
    'Фейл',
    'Вин',
    'Уныние',
    'Чёткая волна',
    'Ебаный стыд',
    'бубубубу',
    'Успешное',
    'Unspecified description',
    'Неуловимое',
    'Butthurt.fm',
    'Сарафанное',
    'Юбка Керенского',
    'Фимоз',
    'Аматорное',
    'Раковые шейки',
    'Понтовое',
    'Озабоченное',
    'Интерпрайз',
    'FreeBSD',
    'LOVE :*',
    'Настоящее',
    'Которое не хочется выключать',
    'Segmentation fault',
    'Ночные гадости',
    'ВЫКЛЮЧИЛ',
    'Лютое',
    'Обновленное',
    'Школополис',
    'Последний оплот',
    'Овал',
    '\'title\' is not a string',
    'Элитное',
    'Весёлый молочник',
    'В 561 раз больше',
    'Безюбилейное',
    'Единственное',
    'Краски.фм',
    'RYTP',
    'САС',
    'Нуждики',
    'Экспериментальное',
    'Undefined behavior',
];
