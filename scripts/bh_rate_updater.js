/* global perc2color, fetchPath */
/* eslint no-unused-vars:0 */

/* jshint unused:false */

/**
 * Создает объект рейтинга
 * @constructor
 */
function Rating() {
    this.value = 0;
    this.color = 'initial';
    this.elementId = '';
}

/**
 * Создает рейтинг успешности трека
 * @param {string} value
 * @extends {Rating}
 * @constructor
 */
function TrackSuccess(value) {
    const parsedValue = parseInt(value);
    const rating = new Rating();
    rating.value = parsedValue * 2 - 100;
    rating.color = perc2color(parsedValue);
    rating.elementId = 'trackSuccessValue';
    return rating;
}

/**
 * Создает рейтинг батхерта
 * @param {string} value
 * @extends {Rating}
 * @constructor
 */
function Butthurt(value) {
    const parser = new DOMParser();
    const butthurtHtml = parser.parseFromString(value, 'text/html');
    const rating = new Rating();
    rating.value = parseInt(butthurtHtml.body.textContent.slice(0, -1));
    rating.color = perc2color(100 - rating.value);
    rating.elementId = 'butthurtValue';
    return rating;
}

/**
 * Контроллер рейтинга
 * @constructor
 */
function RatingController() {

}

/**
 * Создает объект рейтинга из текста
 * @param {Response} response
 * @param {typeof Rating} RatingType
 * @returns {Promise<Rating>}
 * @private
 */
RatingController.prototype._constructRating = function (response, RatingType) {
    return response.text()
        .then(text => new Promise(resolve => resolve(new RatingType(text))));
};

/**
 * Показывает элементы рейтинга
 * @param {Rating} rating
 * @private
 */
RatingController.prototype._showRating = function (rating) {
    const ratingElement = document.getElementById(rating.elementId);
    ratingElement.textContent = `${rating.value}%`;
    ratingElement.style.color = rating.color;
};

/**
 * Обновляет рейтинг
 */
RatingController.prototype.updateRating = function () {
    fetchPath('./data/bh.html')
        .then(response => this._constructRating(response, Butthurt))
        .then(this._showRating, console.error);
    fetchPath('./data/rate.js')
        .then(response => this._constructRating(response, TrackSuccess))
        .then(this._showRating, console.error);
};
