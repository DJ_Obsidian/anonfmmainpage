/* Свалка из полезных в быту функций и ГЛАВНЫХ_НАСТРОЕК */

/* global showdown */
/* eslint no-unused-vars:0 */

/* jshint unused:false */

const MAIN_SETTINGS = {
    useMskTimezone: true,
    selectedStream: 0,
    newPostsOnTop: true,
    useMarkdown: true,
    show: {
        topPanel: true,
        loremIpsum: true,
        clock: true,
        feed: true,
        schedule: true,
    },
};

/** #YOLO
 * https://ru.wikipedia.org/wiki/Monkey_patch
 * Утащеный с https://stackoverflow.com/a/7616484 код для считания хешей строки
 * @returns {number}
 */
String.prototype.hashCode = function () {
    return this
        .split``
        .map(chr => chr.charCodeAt``)
        .reduce((hash, chr) => {
            hash = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
            return hash;
        }, 0);
};

/**
 * Обрезает определённый символ с обеих сторон в строке
 * @param {String} char
 */
/* String.prototype.trimChar = function (char) {
    char = char.toString();

    let begin = 0;
    let end = this.length - 1;

    while (this.charAt(begin) === char)
        begin++;
    while (this.charAt(end) === char && begin < end)
        end--;

    return this.substring(begin, end + 1);
}; */

/**
 * Утащен с https://stackoverflow.com/a/33906108
 * Код для выбора случайного элемента массива
 * @returns {Object}
 */
Array.prototype.sample = function () {
    return this[Math.floor(Math.random() * this.length)];
};

/**
 * Тупейшая заshitа от XSS
 * Рекурсивно заменяет в строках '<' на '&lt;'
 * А всё потому что в некоторых местах __пока__ призодится использовать .innerHTML вместо .textContent
 * @param {Array|string} input
 * @returns {Promise<Array|string>}
 */
// function stupidSanitizer(input) {
//     // Осторожно, модерн^W рекурсия!
//     function seniorStupidSanitizer(input) {
//         switch (typeof input) {
//             case 'object':
//                 // Получаем из объекта все его поля и рекурсивно по ним проходимся
//                 Object.keys(input).forEach(key => input[key] = seniorStupidSanitizer(input[key]));
//                 break;
//             case 'string':
//                 input = input.replace(/</g, '&lt;');
//                 break;
//         }
//         return input;
//     }
//
//     return new Promise(resolve => resolve(seniorStupidSanitizer(input)));
// }

/**
 * Пытается преобразовывать MarkDown в HTML
 * @param {string} text
 * @returns {string}
 */
function tryParseMd(text) {
    if (text.startsWith('%md')) {
        text = text.substr(3).trim(); // Делаем обрезание (сигнатуры)
        return new showdown.Converter().makeHtml(text);
    }
    return text;
}

/**
 * Кричит в сосноль в случае ошибок
 * @param {string} url
 * @param {number} status
 * @param {string} statusText
 */
// function reportError(url, status, statusText) {
//     console.error(`При попытке запроса ${url} произошла ошибка! Код возврата: ${status}, ${statusText}`);
// }

/**
 * Преобразует некоторый период в строку вида "4 часа 5 минут"
 * @param {number} begin
 * @param {number|null} end
 * @returns {string}
 */
function formatDuration(begin, end = null) {
    function __getTimeString(number, variants) {
        const hun = number % 100;
        const dec = number % 10;

        if (hun >= 11 && hun <= 14 || dec === 0) {
            return variants.many; // x[11,14], x0
        }
        if (dec === 1) {
            return variants.one; // x1
        }
        if (dec <= 4) {
            return variants.two; // x[2,4]
        }
        return variants.many; // x[5,9]
    }

    function _getTimeString(number, variants) {
        if (!Number.isInteger(number)) {
            return '';
        }
        return `${number} ${__getTimeString(number, variants)}`;
    }

    function getMinutesString(number) {
        return _getTimeString(number, {
            one: 'минута',
            two: 'минуты',
            many: 'минут',
        });
    }

    function getHoursString(number) {
        return _getTimeString(number, {
            one: 'час',
            two: 'часа',
            many: 'часов',
        });
    }

    /* Точка входа */
    let ms = begin;
    if (end) {
        ms = end - begin;
    }

    let hours = Math.floor(ms / 1000 / 60 / 60);
    let minutes = Math.round(ms / 1000 / 60 % 60);
    if (minutes === 60) {
        hours += 1;
        minutes = 0;
    }

    let result;
    if (hours > 0 && minutes > 0) {
        result = `${getHoursString(hours)} ${getMinutesString(minutes)}`;
    } else if (hours > 0) {
        result = `${getHoursString(hours)}`;
    } else {
        result = `${getMinutesString(minutes)}`;
    }
    return result;
}

/**
 * Форматирует число в размер
 * @param {number} bytes
 * @param {boolean} useBi
 */
function formatSize(bytes, useBi = false) {
    const prefixes = useBi ? ['Б', 'КиБ', 'МиБ', 'ГиБ', 'ТиБ'] : ['Б', 'КБ', 'МБ', 'ГБ', 'ТБ'];
    const divisor = useBi ? 1024 : 1000;

    let prefixNumber = 0;
    while (bytes >= divisor) {
        bytes /= divisor;
        prefixNumber++;
    }

    const n = Math.round(bytes * 10) / 10;
    return `${n} ${prefixes[prefixNumber]}`;
}

/**
 * Тот же setInterval, только исполняющий коллбек сразу
 * @param {TimerHandler} handler
 * @param {number} timeout
 * @returns {number}
 */
function setIntervalImmediately(handler, timeout) {
    handler();
    return setInterval(handler, timeout);
}

/**
 * Запускает обновляторов содержимого из переданного массива
 * @param {<string|number>[]} contentUpdaters
 */
function startContentUpdaters(contentUpdaters) {
    contentUpdaters.forEach(contentUpdater =>
        setIntervalImmediately(contentUpdater.updater, contentUpdater.timeout * 1000)
    );
}

/**
 * Загружает url, кидает ошибку выполнения в случае неудачи
 * @param {string} url
 * @returns {Promise<Response>}
 * @throws {Error}
 */
function fetchPath(url) {
    return fetch(url)
        .then(response => {
            if (response.ok) {
                return Promise.resolve(response);
            }
            return Promise.reject(new Error(
                `При попытке запроса ${response.url} произошла ошибка! ` +
                `Код возврата: ${response.status}, ${response.statusText}`
            ));
        });
}

/**
 * Пиздит урл, возвращает массив объектов
 * @param {string} url
 * @param {function} serializer
 * @returns {Promise<Object[]>}
 */
function fetchSerializeJson(url, serializer) {
    /**
     * Преобразует массив говна в массив объектов
     * @param {Response|Array} rawArray
     * @param {function} Serializer
     * @returns {Promise<Object[]>}
     * @private
     */
    function _serializer(rawArray, Serializer) {
        return new Promise(resolve => resolve(rawArray.map(raw => new Serializer(raw))));
    }

    return fetchPath(url)
        .then(response => response.json(), reason => {
            console.error(reason);
            return [];
        })
        .then(json => _serializer(json, serializer));
    // .then(stupidSanitizer) для сообщений защита есть на стороне сервера
}

/**
 * Просто обновляет часики. Зачем-то. Ну потому что
 */
function updateClock() {
    const d = new Date();
    const clock = document.getElementById('clock');
    if (MAIN_SETTINGS.useMskTimezone) {
        const minsToMs = mins => mins * 60 * 1000;
        const hoursToMs = hours => minsToMs(hours * 60);

        const currentMs = Date.now();
        // В России - отрицательные значения. При сложении со смещением даёт время в нужном часовом поясе
        const gmtCorrection = minsToMs(d.getTimezoneOffset());
        // Москва - GMT+3
        const mskOffset = hoursToMs(3);
        d.setTime(currentMs + gmtCorrection + mskOffset);
        clock.textContent = `Московское время: ${d.toLocaleTimeString()}`;
    } else {
        clock.textContent = `Местное время: ${d.toLocaleTimeString()}`;
    }
}

/**
 * Обновляет картинку с названием трека
 */
function updateSongPic() {
    const SongPic = document.getElementById('nowPlaying');
    SongPic.src = `./data/radioanon.png?${new Date().getTime()}`;
}

/**
 * Возвращает цвет в зависимости от процентного значения
 * Взято из https://gist.github.com/mlocati/7210513
 * @param {number} perc
 * @returns {string}
 */
function perc2color(perc) {
    let r, g;
    if (perc < 50) {
        r = 255;
        g = Math.round(5.1 * perc);
    } else {
        g = 255;
        r = Math.round(510 - 5.1 * perc);
    }
    return `rgb(${r}, ${g}, 0)`;
}
