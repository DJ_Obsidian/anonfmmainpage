/* global splashController, fetchPath */
/* eslint no-unused-vars:0 */
/* jshint unused:false */

const DjSplashPrefix = './data';

/**
 * Создает отображение для ссылки на видео
 * @constructor
 */
function VideoView() {
    this.container = document.getElementById('videoContainer');
    this.container.classList.add('hidden');
    this.videoLink = document.createElement('a');
    this.videoLink.appendChild(document.createTextNode('Открыть видеопоток'));
    this.container.appendChild(this.videoLink);
}

/**
 * Устанавливает ссылку на видео
 * @param link
 */
VideoView.prototype.setLink = function (link) {
    if (link) {
        this.container.classList.remove('hidden');
        this.videoLink.href = link;
    } else {
        this.container.classList.add('hidden');
    }
};

/**
 * Создает отображение для кнопки звонка
 * @constructor
 */
function CallView() {
    this.container = document.getElementById('callContainer');
    this.container.classList.add('hidden');
    this.callLink = document.createElement('a');
    this.callLink.appendChild(document.createTextNode('Позвонить диджею'));
    this.container.appendChild(this.callLink);
}

/**
 * Устанавливает ссылку для звонка
 * @param link
 */
CallView.prototype.setLink = function (link) {
    if (link) {
        this.container.classList.remove('hidden');
        this.callLink.href = link;
    } else {
        this.container.classList.add('hidden');
    }
};

/**
 * Создает отображение для оповещений
 * @constructor
 */
function AnnouncementView() {
    this.container = document.getElementById('announcementContainer');
    this.container.classList.add('hidden');
}

/**
 * Добавляет оповещение
 * @param announcementText
 */
AnnouncementView.prototype.addAnnouncement = function (announcementText) {
    let announcementBlock = document.createElement('div');
    let closeButton = document.createElement('button');
    let announcementContent = document.createTextNode(announcementText);
    closeButton.classList.add('closeIcon');

    /**
     * Удаляет блок оповещения
     * @private
     */
    const _removeAnnouncement = () => {
        announcementBlock.remove();
        if (this.container.children.length === 0) {
            this.container.classList.add('hidden');
        }
        closeButton.removeEventListener('click', _removeAnnouncement);
        announcementBlock = null;
        closeButton = null;
        announcementContent = null;
    };

    closeButton.addEventListener('click', _removeAnnouncement);
    announcementBlock.appendChild(closeButton);
    announcementBlock.appendChild(announcementContent);
    this.container.appendChild(announcementBlock);
    this.container.classList.remove('hidden');
};

/**
 * Создает данные об индикаторах
 * @param {JSON} value
 * @param {string} value.video
 * @param {string} value.splash
 * @param {string} value.call
 * @param {string} value.ann
 * @constructor
 */
function Indicators({video: videoLink, splash: splashLink, call: callLink, ann: announcement}) {
    this.videoLink = videoLink;
    this.splashLink = splashLink;
    this.callLink = callLink;
    this.announcements = announcement;
}

/**
 * Создает контроллер индикаторов на странице
 * @constructor
 */
function IndicatorsController() {
    this.indicators = new Indicators({});
    this.callView = new CallView();
    this.videoView = new VideoView();
    this.announcementView = new AnnouncementView();
}

/**
 * Производит смену сплеша
 * @private
 */
IndicatorsController.prototype._showSplash = function () {
    if (this.indicators.splashLink) {
        splashController.setSplash(DjSplashPrefix + this.indicators.splashLink);
    } else {
        splashController.loadDefaultSplash();
    }
};

/**
 * Создает объект индикаторов из запроса
 * @param {Response} response
 * @returns {Promise<Indicators>}
 * @private
 */
IndicatorsController.prototype._constructIndicators = function (response) {
    return Promise.resolve(response.json())
        .then(text => new Promise(resolve => resolve(new Indicators(text))));
};

/**
 * Отображает новые индикаторы
 * @param {Indicators} indicators
 * @private
 */
IndicatorsController.prototype._showIndicators = function (indicators) {
    if (this.indicators.videoLink !== indicators.videoLink) {
        this.indicators.videoLink = indicators.videoLink;
        this.videoView.setLink(this.indicators.videoLink);
    }
    if (this.indicators.callLink !== indicators.callLink) {
        this.indicators.callLink = indicators.callLink;
        this.callView.setLink(this.indicators.callLink);
    }
    if (this.indicators.splashLink !== indicators.splashLink) {
        this.indicators.splashLink = indicators.splashLink;
        this._showSplash();
    }
    if (this.indicators.announcements !== indicators.announcements) {
        this.indicators.announcements = indicators.announcements;
        this.announcementView.addAnnouncement(indicators.announcements);
    }
};

/**
 * Обновляет индикаторы
 */
IndicatorsController.prototype.updateIndicators = function () {
    fetchPath('./data/info.js')
        .then(response => this._constructIndicators(response))
        .then(indicators => this._showIndicators(indicators), console.error);
};
