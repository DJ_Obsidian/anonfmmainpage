/**
 * Создает картинку для отрисовки
 * @param {string} path
 * @extends {HTMLImageElement}
 * @property {string} path
 * @property {string} dir
 * @constructor
 */
function SplashImage(path) {
    const image = new Image();
    image.path = path;

    /**
     * Загружает картинку
     * @function
     */
    image.loadImage = () => {
        image.src = image.path;
    };
    image.id = 'splashImage';
    return image;
}

/**
 * Создает контроллер картинки на сплеше
 * @constructor
 */
function SplashController() {
    this._initSplash();
    this.loadDefaultSplash();
}

/**
 * Устанавливает картинку по умолчанию на сплеш
 */
SplashController.prototype.loadDefaultSplash = function () {
    document.getElementById('splashImage').replaceWith(this._DefaultImage);
};

/**
 * Меняет сплеш на случайный из списка
 * @private
 */
SplashController.prototype._changeSplash = function () {
    const splashImagePath = document.getElementById('splashImage').path;
    const newSplashPath = this._SplashList
        .filter(path => (this._SplashDir + path) !== splashImagePath)
        .sample();
    this.setSplash(this._SplashDir + newSplashPath);
};

/**
 * Устанавливает начальные настройки сплеша
 * @private
 */
SplashController.prototype._initSplash = function () {
    const splashResetButton = document.getElementById('splashResetButton');
    splashResetButton.addEventListener('click', () => this.loadDefaultSplash());

    this._LoadingAnimation = this._loadImage(this._SplashDir + this._LoadingSplash);
    this._DefaultImage = this._loadImage(this._SplashDir + this._DefaultSplash);
    this._DefaultImage.addEventListener('click', () => this._changeSplash());
};

/**
 * Устанавливает картинку на сплеш по указанному пути
 * @param {string} path
 */
SplashController.prototype.setSplash = function (path) {
    const splashImage = document.getElementById('splashImage');
    const newSplashImage = this._loadImage(path);
    splashImage.replaceWith(this._LoadingAnimation);

    const _newSplashLoad = () => {
        this._LoadingAnimation.replaceWith(newSplashImage);
        newSplashImage.addEventListener('click', () => this._changeSplash());
    };
    const _newSplashError = () => {
        this._LoadingAnimation.replaceWith(splashImage);
    };

    newSplashImage.addEventListener('load', _newSplashLoad);
    newSplashImage.addEventListener('error', _newSplashError);
};

/**
 * Загружает картинку для сплеша
 * @param {string} path
 * @returns {HTMLImageElement}
 * @private
 */
SplashController.prototype._loadImage = function (path) {
    const image = new SplashImage(path);
    image.loadImage();
    return image;
};

/**
 * Список случайных картинок для сплеша
 * @type {string[]}
 * @private
 */
SplashController.prototype._SplashList = [
    'bus.jpg',
    'mac.jpg',
    'cloudtags.png',
    'dostav.jpg',
    'prof.jpg',
];

/**
 * Директория случайных картинок для сплеша
 * @type {string}
 * @private
 */
SplashController.prototype._SplashDir = './media/splashimages/';

/**
 * Картинка с анимацией загрузки нового сплеша
 * @type {string}
 * @private
 */
SplashController.prototype._LoadingSplash = 'loading.gif';

/**
 * Картинка по умолчанию
 * @type {string}
 * @private
 */
SplashController.prototype._DefaultSplash = 'geomap.jpg';
